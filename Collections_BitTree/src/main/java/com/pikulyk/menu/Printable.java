package com.pikulyk.menu;

@FunctionalInterface
public interface Printable {
  void print();
}
