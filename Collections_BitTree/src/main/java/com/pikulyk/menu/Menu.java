package com.pikulyk.menu;

import com.pikulyk.bintree.BinTree;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

  public class Menu {

    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;
    private static Scanner input = new Scanner(System.in);
    private static Scanner inputInt = new Scanner(System.in);
    private BinTree<Integer, String> binTree = new BinTree<>();

    public void myMenu() {
      menu = new LinkedHashMap<>();
      menuMethods = new LinkedHashMap<>();
      menu.put("1", "1 - Add new value");
      menu.put("2", "2 - Get value by key");
      menu.put("3", "3 - Remove value by key");
      menu.put("4", "4 - Check if contains key");
      menu.put("5", "5 - Check if Binary Tree is empty");
      menu.put("Q", "Q - Exit");

      menuMethods.put("1", this::put);
      menuMethods.put("2", this::get);
      menuMethods.put("3", this::remove);
      menuMethods.put("4", this::containsKey);
      menuMethods.put("5", this::isEmpty);
    }

    private void outputMenu() {
      System.out.println("--------");
      System.out.println("| MENU |");
      System.out.println("--------");
      for (String string : menu.values()) {
        System.out.println(string);
      }
    }

    private void put() {

    }
    private void get() {

    }
    private void remove() {

    }
    private void containsKey() {

    }
    private void isEmpty() {

    }

    public void show() {
      String keyMenu;
      do {
        outputMenu();
        System.out.print("Please select menu point: ");
        keyMenu = input.nextLine().toUpperCase();
        try {
          menuMethods.get(keyMenu).print();
        } catch (Exception e) {
          e.printStackTrace();
        }
      } while (!keyMenu.equals("Q"));
    }
}
